var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001,
  mongoose = require('mongoose'),
	bodyParser = require('body-parser');
	
require('./api/models/todoListModel.js');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://mongo:27017/profiles', {
	useMongoClient: true
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/todoListRoutes');
routes(app);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);

console.log('RESTAPI server started on: ' + port);
