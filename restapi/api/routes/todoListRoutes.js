'use strict';

module.exports = function(app) {
	var todoList = require('../controllers/todoListController')

	app.route('/profiles')
		.get(todoList.getAllProfiles)

	app.route('/profile')
		.put(todoList.addProfile)

	app.route('/profile/:id')
		.get(todoList.getProfile)
		.post(todoList.editProfile)
		.delete(todoList.delProfile)
}