'use strict';

var mongoose = require("mongoose")
var Profile = mongoose.model("Profile");
	
exports.getAllProfiles = function(req, res) {
  Profile.find({}, function(err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

exports.addProfile = function(req, res) {

	var newProfile = new Profile(req.body);
	
  newProfile.save(function(err, profile) {
		if (err) res.json('KO');
		res.json(profile);
  });
}

exports.getProfile = function(req, res) {
	Profile.findById(req.params.id, function(err, profile) {
		if (err)
			res.send(err);
		res.json(profile);
	});
}

exports.editProfile = function(req, res) {
	Profile.findOneAndUpdate({_id:req.params.id}, req.body, {new: true}, function(err, profile) {
    if (err)
      res.send(err);
    res.json('ok');
  });
}

exports.delProfile = function(req, res) {
	Profile.remove({
		_id: req.params.id
	}, function(err, profile) {
		if (err)
			res.send(err);
		res.send('OK');
	});
}