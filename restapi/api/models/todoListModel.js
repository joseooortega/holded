'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Profile = new Schema({
	imagen: {
    type: String,
    default: "/src/template/media/profile.jpg"
  },
  nombre: String,
  apellidos: String,
  correo: String,
  cargo: String,
  centro: String,
  salario: Number,
  horas: Number
});

module.exports.tasks = mongoose.model('Profile', Profile);