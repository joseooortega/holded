<?php
session_start();

// TODO: Utilizar esta variable de sesión para mostrar más logs
$_SESSION['debug'] = TRUE;
// TODO: Definir variables globales. Ej: ruta base a restapi
$restUrl = 'my-rest:3001/';

include './helpers.php';
include './src/includes/template.php';