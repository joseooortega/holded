<?php

include './helpers.php';

$todo = !empty($_POST['todo']) ? $_POST['todo'] : NULL;

switch ($todo) {
	case 'edit':
	case 'add':
		manageProfile($todo);
		break;
	case 'delete':
		deleteProfile();
	case 'preparecontent':
		if (!empty($_POST['name']) && !empty($_POST['value'])) {
			$name = cleanVar($_POST['name']);
			$value = cleanVar($_POST['value']);
			die(prepareContent($name, $value));
		}
	case 'fieldlist':
		die(json_encode(getFieldList()));
	default:
		die('You have nothing to do here');
		break;
}


function manageProfile($todo) {
	
	$errores = [];
	$profileData = [];
	$validKeys = ["id", "nombre", "apellidos", "correo", "cargo", "centro", "salario", "horas"];

	if (!empty($_POST)) {
		foreach ($_POST as $key => $value) {
			if (in_array($key, $validKeys)) {
				$profileData[$key] = trim(strip_tags($value));
			}
		}
	}

	if ($todo == 'edit' && empty($profileData['id'])) {
		$errores[] = 'There was a problem with profile data';
	}

	$target_dir = "./src/files/" . $profileData['id'] . '/';

	$results = uploadFiles($target_dir, ['png', 'jpg', 'gif', 'jpeg']);

	foreach ($results as $imageKey => $value) {
		if ($value == 'OK') {
			$profileData['imagen'] = $target_dir . $imageKey;
			break;
		}
	}

	if ($todo == 'edit') {
		if (!editProfile($profileData)){
			$errores[] = "Could not modify the profile";
		}
	} else if ($todo == 'add') {
		$response = createProfile($profileData);
		if ($response == 'KO') {
			die('h2');
			$errores[] = "Could not modify the profile";
		} else {
			$response['id'] = $response['_id'];
			unset($response['_id']);
			unset($response['__v']);
			$profileData = $response;
		}
	}


	$return = [
		'errores' => $errores,
		'data' => $profileData
	];
	die(json_encode($return));
}
function createProfile($profileData){
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'my-rest:3001/profile/' . $profileData['id']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($profileData));
	return json_decode(curl_exec($ch), TRUE);
}
function editProfile($profileData){
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'my-rest:3001/profile/'.$profileData['id']);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($profileData));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = json_decode(curl_exec($ch));
	return !empty($result) && $result == 'ok' ? TRUE : FALSE;
}
function deleteProfile(){

	if (!empty($_POST['id'])) {

		$id = trim(strip_tags($_POST['id']));

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'my-rest:3001/profile/'.$id);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		
		die($result);
	}
	die('KO');
}