<?php

function getContent($url){
	try {
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
		curl_close($ch);
		return json_decode($output, TRUE);

	} catch (\Exception $e) {
		if ($_SESSION['debug']) die($e->getMessage());
		return [];
	}
}

function getLabel($name) {
	switch ($name) {
		case 'correo':
			return 'Correo electrónico';
		case 'horas':
			return 'Horas semanales';
		case 'imagen':
			return '';

		default:
			return ucfirst($name);
	}
}

function getFieldList() {
	
	/*----------------------------
		Lo mejor sería obtener esta lista a través de restapi. Pero no he sido capaz de conseguir esta lista mediante mongodb.
	----------------------------*/

	return [
		'imagen' => 'image',
		'nombre' => 'string',
		'apellidos' => 'string',
		'correo' => 'string',
		'cargo' => 'string',
		'centro' => 'string',
		'salario' => 'number',
		'horas' => 'number'
	];
}

function prepareContent($name, $value){
	switch ($name) {
		case 'correo':
			return '<a href="mailto:' . $value . '">' . $value . '</a>';
			break;
		case 'imagen':
			return '<img src="'.$value.'" alt="Imagen de perfíl">';
			break;
		
		default:
			return $value;
			break;
	}
}

function uploadFiles($target_dir, $availableFormats) {
	try {

		$result = [];
		
		if (!empty($_FILES)) {

			if (!file_exists($target_dir))
				mkdir($target_dir, 0777, true);

			foreach ($_FILES as $key => $file) {
				$name = str_replace([' '], ['_'], strtolower(basename($file["name"])));
				$target_file = $target_dir . $name;
				$check = getimagesize($file["tmp_name"]);

				if ($check !== false) {
					$mime = explode('/', $check["mime"]);
					$mime = $mime[1];
					if (!in_array($mime, $availableFormats)) {
						$result[$name] = "The image doesn't have a valid format";
						continue;
					}
				} else {
					$result[$name] = "File is not an image.";
					continue;
				}

				// Check file size
				if ($file["size"] > 500000) {
					$result[$name] = "Your file is too large.";
					continue;
				}

				if (move_uploaded_file($file["tmp_name"], $target_file)) {
					// echo "The file " . basename($file["name"]) . " has been uploaded.";
					$result[$name] = 'OK';
				} else {
					$result[$name] =  "There was an error uploading your file.";
				}
			}
		}

		return $result;
	} catch (\Exception $e) {
		return $e->getMessage();
	}
}

function cleanVar($value) {
	return trim(strip_tags($value));
}