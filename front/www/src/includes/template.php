<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="/src/template/css/styles.css">
	<script src="/src/template/js/common.js"></script>
	<title>Profile list</title>
</head>

<body>
	<?php include 'header.html' ?>
	<div id="content">
		<div class="container">
			<?php
				include 'operations.html';
				include 'content.php'
			?>
		</div>
		<div id="popup">
			<?php include 'form.html' ?>
		</div>
	</div>
	<?php include 'footer.html' ?>
</body>

</html>