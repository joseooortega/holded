<?php

$content = getContent($restUrl.'profiles');
$fieldlist = getFieldList();

echo '<ul class="profile_list">';

if (empty($content)) {
	include 'no_content_message.html';
}

foreach ($content as $profile) {
	echo '<li data-id="'. $profile['_id'].'">';

	unset($profile['_id']);
	unset($profile['__v']);

	/*----------------------------
		Recorro los campos en vez de las propiedades del perfil para asegurar el orden de las columnas
	----------------------------*/

	foreach ($fieldlist as $name => $type) {
		echo '<div class="property ' . strtolower($name) . '">';
			echo '<label>' . getLabel($name) . '</label>';
			echo '<div class="value">' . prepareContent($name, $profile[$name]) . '</div>';
		echo '</div>';
	}
	
	// foreach ($profile as $name => $property) {
	// 	echo '<div class="property '. strtolower($name).'">';
	// 		echo '<label>'.getLabel($name).'</label>';
	// 		echo '<div class="value">'.prepareContent($name, $property).'</div>';
	// 	echo '</div>';
	// }

	echo '<div class="property operations">';
		echo '<label>Acciones</label>';
		echo '<div class="value">';
			echo '<div class="edit" title="Edita el perfil"></div>';
			echo '<div class="delete" title="Elimina el perfil"></div>';
		echo '</div>';
	echo '</div>';

	echo '</li>';
}
echo '</ul>';