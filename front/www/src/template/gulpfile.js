const gulp = require('gulp'),
 sass = require('gulp-sass'),
 autoprefixer = require('gulp-autoprefixer'),
 sourcemaps = require('gulp-sourcemaps'),
 browserSync = require('browser-sync').create(),
 reload = browserSync.reload(),
 concat = require('gulp-concat'),
 pump = require('pump');


gulp.task('sync', () => {
	browserSync.init({
		proxy: {
			target: "localhost"
		},
		open: false
	})
});

gulp.task('sass', (cb) => {
	pump([
		gulp.src('./scss/*.scss'),
		sourcemaps.init(),
			sass({
				outputStyle: 'compressed',
				//sourceComments: true
			}),

			autoprefixer({
				// browsers: ['last 2 versions'],
				// grid: true,
			}),
			sourcemaps.write('./maps'),
		gulp.dest('./css'),
		browserSync.stream()
	], cb)
});

gulp.task('watch', ['sync', 'sass'], ()=> {
	console.log('asdasd')
	gulp.watch('./scss/*.scss', ['sass']);
})

gulp.task('default',['sync', 'sass', 'watch']);