

/*----------------------------
	EVENTS
----------------------------*/
// Document ready
document.addEventListener("DOMContentLoaded", function() {
	addProfile();
	editProfile();
	deleteProfile();
	closeForm();
	fileInput();

	// Form submit
	document.getElementById("profile_form").addEventListener('submit', sendProfileSubmit)

	// Show messages
	var msg = document.getElementsByClassName('message')
	Array.prototype.forEach.call(msg, function(el) {
    fadeIn(el);
  });
})


/*----------------------------
	FUNCTIONS
----------------------------*/
function addProfile(){
	document.getElementsByClassName('new')[0].addEventListener('click', function() {
		showForm();
		document.getElementById("profile_form").classList.add('add_form')
	})
}
function editProfile(){

	const els = document.getElementsByClassName('edit');
	const form = document.getElementById("profile_form");

	if (els.length == 0) return;

	Array.prototype.forEach.call(els, function(el) {
    el.addEventListener("click", function(e) {
      form.classList.add("edit_form");
      var id = e.currentTarget.closest("li").getAttribute("data-id");
      showForm(id);
    });
  });
}
function deleteProfile() {

	const els = document.getElementsByClassName("delete");
	
	if (els.length == 0) return;

	Array.prototype.forEach.call(els, function(el) {
    el.addEventListener("click", function(e) {
      var target = e.currentTarget.closest("li");
      var id = target.getAttribute("data-id");
      var xhr = new XMLHttpRequest();
      var formData = new FormData();

      formData.append("todo", "delete");
      formData.append("id", id);

      xhr.open("POST", "/ajax-requests.php");
      xhr.send(formData);
      xhr.onload = function() {
        if (xhr.status == 200) {
          if (xhr.response == "OK") {
            fadeOut(target);
            setTimeout(function() {
              target.remove();
            }, 200);
          } else {
            showMessage("warning", "Could not delete the profile", true);
          }
        } else {
          showMessage("warning", "Could not delete the profile", true);
        }
      };
    });
  });
}
function closeForm() {
	document.getElementById("popup").querySelector('.close').addEventListener('click', function() {
		hideForm();
	})
}
function fileInput() {

	const form = document.getElementById("profile_form");
	
  form.querySelectorAll('input[type="file"]').forEach(function(el) {
    el.addEventListener("change", function(e) {
      var label = e.currentTarget.nextElementSibling;
      var url = e.currentTarget.value;

      if (url.length == 0) {
        label.innerHTML = label.getAttribute("data-default");
      } else {
        url = url.split("\\");
        let fileName = url[url.length - 1];
        let format = fileName.split(".");
        let imageFormats = ["png", "jpg", "gif", "jpeg"];
        format = format[format.length - 1].toLowerCase();
        
        if (imageFormats.indexOf(format) == -1) {
          label.innerHTML = label.getAttribute("data-default");
          form.classList.add("disabled");
          e.currentTarget.value = "";
          return;
        }

        form.classList.remove("disabled");

        label.innerHTML = fileName;
      }
    });
  });
}


/*----------------------------
	HELPERS
----------------------------*/
function showForm(id) {

	const form = document.getElementById("profile_form")
	const button = form.getElementsByTagName('button')[0]

	if (id) {
		const profile = document.querySelector('li[data-id="'+id+'"]')
		
		if (profile.length == 0) return;

		form.querySelector('input[type="hidden"]').value = id;
		form.getElementsByTagName('img')[0].setAttribute('src', profile.getElementsByTagName('img')[0].getAttribute('src'))

		form.querySelectorAll('input:not([type="hidden"]):not([type="file"])').forEach(function(el) {
			var name = el.getAttribute('name');
			el.value = profile.querySelector('.'+name+' .value').textContent
		})

		button.innerHTML = button.getAttribute('data-edittext')
	} else {
		button.innerHTML = button.getAttribute('data-newtext')
	}
	
	fadeIn(form.parentNode);
	document.getElementsByTagName("body")[0].classList.add("no_scroll");
}
function hideForm(){

	const form = document.getElementById("profile_form");

	fadeOut(document.getElementById("popup"));
	document.getElementsByTagName('body')[0].classList.remove('no_scroll')
	form.classList.remove("edit_form", "add_form");
	form.querySelectorAll('input').forEach(function(item) {
		item.value = ''
	})
}
function sendProfileSubmit(e) {

	e.preventDefault();
	const form = e.currentTarget

	if (form.classList.contains('disabled')) return;

	todo = form.classList.contains('edit_form') ? 'edit' : 'add';

	var xhr = new XMLHttpRequest();
	var formData = new FormData();

	let photo = form.querySelector('input[name="imagen"]').files[0];
	if (photo !== undefined)
  	formData.append("photo", photo);
	
	form.querySelectorAll('input:not([type="file"])').forEach(function(el) {
		formData.append(el.getAttribute("name"), el.value);
	})

	formData.append("todo", todo);

  xhr.open("POST", "/ajax-requests.php");
  xhr.send(formData);
  xhr.onload = function() {
    if (xhr.status == 200) {
			
			var response = JSON.parse(xhr.response);

			if (todo == 'edit'){

        if (response.errores.length == 0) {
          let id = form.querySelector('input[name="id"]').value;
          const profile = document.querySelector('li[data-id="' + id + '"]');

          for (const key in response.data) {
            let field = profile.querySelector(".property." + key + " .value");
            if (field !== null)
              prepareContent(key, response.data[key], function(text) {
                field.innerHTML = text;
              });
          }
        }

			} else {

				console.log(xhr.response);
				if (response.errores.length == 0) {

					var li = document.createElement('li')
					li.setAttribute("data-id", response.data.id)

					getFieldList(function(fields) {
            for (const name in fields) {
              var div = document.createElement("div");
              var label = document.createElement("label");
              var value = document.createElement("div");

              div.classList.add("property", name);

              label.innerHTML = getLabel(name);

              value.classList.add("value");
              prepareContent(name, response.data[name], function(text) {
                value.innerHTML = text;
              });

              div.appendChild(label);
              div.appendChild(value);

              li.appendChild(div);
            }
          });

					document.getElementsByClassName("profile_list")[0].appendChild(li)
				}
			}

			 if (response.errores.length) {
         response.errores.forEach(function(error) {
           showMessage("warning", error);
         });
       }
			
			hideForm();
    }
  };
}
function fadeOut(el){
  el.style.opacity = 1;

  (function fade() {
    if ((el.style.opacity -= .1) < 0) {
      el.style.display = "none";
    } else {
      requestAnimationFrame(fade);
    }
	})();
	
	return el;
}
function fadeIn(el, display){
  el.style.opacity = 0;
  el.style.display = display || "block";

  (function fade() {
    var val = parseFloat(el.style.opacity);
    if (!((val += .1) > 1)) {
      el.style.opacity = val;
      requestAnimationFrame(fade);
    }
	})();
	
	return el;
}
function showMessage(type, message, clean) {
	const messageBox = document.getElementById("message_box");

	if (clean) {
		messageBox.querySelectorAll('.message').forEach(function(item) {
			fadeOut(item)
		})
	}

	var div = document.createElement('div');
	div.classList.add('message', type)
	div.innerHTML = message
	messageBox.appendChild(div)
	setTimeout(function() {
		fadeIn(div);
	}, 200);
}
function getLabel(label) {
	switch (label) {
    case "correo":
      return "Correo electrónico";
    case "horas":
      return "Horas semanales";
    case "imagen":
      return "";
    default:
      return label.toUpperCase()
  }
}
function prepareContent(name, value, cb) {
	var xhr = new XMLHttpRequest();
	var formData = new FormData();
	var r = ''

	formData.append("todo", "preparecontent");
  formData.append("name", name);
  formData.append("value", value);

  xhr.open("POST", "/ajax-requests.php");
  xhr.send(formData);
	xhr.onload = function() {
		if (xhr.status == 200) {
			cb(xhr.response);
		}
	}
}
function getFieldList(cb) {
	var xhr = new XMLHttpRequest();
  var formData = new FormData();

  formData.append("todo", "fieldlist");

  xhr.open("POST", "/ajax-requests.php");
  xhr.send(formData);
  xhr.onload = function() {
    if (xhr.status == 200) {
      cb(JSON.parse(xhr.response));
    }
  };
}


/*----------------------------
	Polyfills
----------------------------*/
// closest function
if (window.Element && !Element.prototype.closest) {
  // https://gomakethings.com/a-native-vanilla-javascript-way-to-get-the-closest-matching-parent-element/
  Element.prototype.closest =
    function (s) {
      var matches = (this.document || this.ownerDocument).querySelectorAll(s),
        i,
        el = this
      do {
        i = matches.length
        while (--i >= 0 && matches.item(i) !== el) { }
      } while ((i < 0) && (el = el.parentElement))
      return el
  }
}

// forEach 
if ('NodeList' in window && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = function (callback, thisArg) {
    thisArg = thisArg || window
    for (var i = 0; i < this.length; i++) {
      callback.call(thisArg, this[i], i, this)
    }
  }
}