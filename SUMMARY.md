# Información del proyecto



### Instalación


- Clonar el [repositorio](https://gitlab.com/joseooortega/holded

		git clone git@gitlab.com:joseooortega/holded.git

- Abrir un terminal con la ruta actual y escribir el siguiente comando:

		docker-compose up

- Abrir el navegador e ir a [localhost](http://localhost/)


### Tareas inacabadas

- Visualizar un nuevo perfil después de rellenar y enviar el formulario. Es necesario actualizar la página para ver el nuevo contenido.
- Hacer pruebas con IE10 / IE11

### Si hubiera tenido más tiempo..

- Añadir un filtro al listado de perfiles para mostrar perfiles de una compañía en concreto.
- Añadir más comentarios en el código.
- Mejorar algunas líneas de código.
- Mejorar el diseño y añadir responsive.
- Eliminar las imágenes de los perfiles eliminados.


### Problemas encontrados

Ya que antes de hacer esta prueba no había jugado con docker y apenas había tocado mongoDB he tenido que hacer un intensivo y aprender estas dos tecnologías entre horas.
A causa de mi poca experiencia también me ha costado bastante tiempo superar los problemas relacionados con docker (sobretodo) y mongodb que me he ido encontrando a lo largo del desarrollo.


### Tecnologías usadas

- PHP + javascript + sass: Frontend
- Node.js: Backend
- Mongodb: Base de datos
- Docker: Entorno

